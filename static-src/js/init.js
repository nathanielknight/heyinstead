let storageKey = 'heyinstead.xyz'

function loadDb() {
    let storage = window.localStorage;
    let src = storage.getItem(storageKey);
    let parsed = JSON.parse(src);
    return parsed;
}


function storeDb(val) {
    let storage = window.localStorage;
    let serialized = JSON.stringify(val);
    storage.setItem(storageKey, serialized);
}


function startApp(cardList) {
    var targetNode = document.getElementById("appbody");
    if (! cardList) {
        var cardList = [];
    }
    var app = Elm.Main.embed(targetNode, { cardList: cardList });
    return app;
}


function init() {
    console.log("Starting `heyinstead`");
    let initialState = loadDb();
    let app = startApp(initialState);
    app.ports.toDB.subscribe(storeDb);
    console.log("Done");
}


window.onload = init;
