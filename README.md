# Heyinstead

An app to fill your attention gap.

See it live at [heyinstead.xyz](https://heyinstead.xyz)


# Building

Requires the following:

- [The Elm platform](http://elm-lang.org/install)
- [Node.js and NPM](https://nodejs.org/en/)
- [The Less Compiler](http://lesscss.org/) (get with `npm install -g lessc`)
- [Ruby/Rake](https://www.ruby-lang.org/en/documentation/installation/) (for building)

With those installed, the following rake tasks build the client:

- `rake elm` to build the Javascript
- `rake less` to build CSS
- `rake static` top copy over static files

Admin tasks for convenience:

- `rake all` to do elm, less, and static
- `rake clean` to clear out compiled files
- `rake serve` to build things and serve them on `localhost:8080`


# Deploying

This app is served with Firebase Hosting. To deploy new content

    rake clean
    rake all
    firebase --project heyinstead deploy --only hosting
