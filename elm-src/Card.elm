module Card exposing (..)

import List
import String

import Html exposing (Html, div, text, a)
import Html.Attributes exposing (style, class, href)

-- Ensure "Link" has a protocol
hasProto : String -> Bool
hasProto url =
    List.any
        (\s -> String.startsWith s url)
        [ "http://"
        , "https://"
        , "mailto:"
        ]

ensureProto : String -> String
ensureProto s =
    if hasProto s
    then s
    else "https://" ++ s


-- Model
type alias Model = {id : Int
                   , text : String
                   , color : String
                   , href : String
                   }

blank : Model
blank =
    { id = 0
    , text = ""
    , color = ""
    , href = ""
    }

-- View
view : Model -> Html msg
view model =
    let
        cardstyle = style [("background-color", model.color)]
    in 
        div [cardstyle, class "card"]
            [a [ href (ensureProto model.href)] [text model.text]]
