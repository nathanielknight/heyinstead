module CardAdder exposing (..)

import Html exposing (Html, div, span, button, input, text)
import Html.Events exposing (onInput, onClick)
import Html.Attributes exposing (value, id, class, style, placeholder)

import Card


-- Utils
colors = [ "#43bf43" -- green
         , "#f34d4d" -- red
         , "#5b5bf1" -- blue
         , "#ffff42" -- yellow
         , "white"
         ]

colorButton : String -> Html Msg
colorButton color =
    span [ style [ ("background-color", color) ]
         , onClick (Update Color color)
         ]
    [text " "]

colorChooser : Html Msg
colorChooser = div
               [id "color-chooser"]
               [ div [id "color-choosers"] <| List.map colorButton colors ]


-- Model
type alias Model = { text : String
                   , color : String
                   , href : String
                   }

empty : Model
empty = {text = "", color = "", href = ""}

toCardModel : Model -> Int -> Card.Model
toCardModel {text, color, href} id =
    { text = text
    , color = color
    , href = href
    , id = id
    }


-- Messages
type Field = Text | Color | Href
type Msg = SendCard Model
         | Update Field String


-- View

cardForm : Model -> Html Msg
cardForm model =
    div
        [id "card-form"]
        [ div [] [ text "Text"
                 , input [ onInput (scratchUpdate Text)
                         , value model.text
                         ]
                       []
                 ]
        , div [] [ text "Link"
                 , input [ onInput (scratchUpdate Href)
                         , value model.href
                         , placeholder "https://"
                         ] []
                 ]
        , div [] [ text "Color"
                 , colorChooser
                 ]
        , button [onClick (SendCard model)] [text "Add"]
        ]

view : Model -> Html Msg
view model =
      div [id "card-adder"]
          [ cardForm model
          , div
                [id "demo-card"]
                [(toCardModel model 0 |> Card.view )]
          ]

scratchUpdate : Field -> String -> Msg
scratchUpdate f s = Update f s


-- Update
update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    let
        send m = (m, Cmd.none)
    in
        case msg of
            SendCard m -> send model  -- this is for interaction
            Update f s -> case f of
                              Text -> send {model | text = s}
                              Href -> send {model | href = s}
                              Color -> send {model | color = s}
