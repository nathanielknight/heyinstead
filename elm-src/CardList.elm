module CardList exposing (..)

import Html exposing (Html, div, button, input, p, text)
import Html.Events exposing (onInput, onClick)
import Html.Attributes exposing (value, class, id)
import List
import Dict
import Maybe
import Random

import Card
import CardAdder


-- Utils
fromCardList : List Card.Model -> Model
fromCardList cs
    =
      let
          cardDict = (List.foldl
                          (\c d -> Dict.insert c.id c d)
                          Dict.empty
                          cs)
      in
          { cards = cardDict
          , scratchCard = CardAdder.empty
          , drawerOpen = False
          , cardOrder = Dict.keys cardDict
          }

toCardList : Model -> List Card.Model
toCardList model
    = model.cards
    |> Dict.toList
    |> List.map (\(_,m) -> m)


without : Int -> List a -> List a
without i xs =
    let
        inits = List.take i xs
        tails = List.drop (i + 1) xs
    in
        inits ++ tails

shuffle : List Int -> Random.Seed -> List Int
shuffle xs step =
    case xs of
        [] -> []
        xs ->
            let
                (i, s) = Random.step (Random.int 0 ((List.length xs) - 1)) step
                ys = without i xs
                x = case (List.head (List.drop i xs)) of
                        Just x -> x
                        Nothing -> 0 -- this should never happen but I can't figure out how to panic
            in
                x::(shuffle ys s)

seeder : Random.Generator Random.Seed
seeder = Random.map Random.initialSeed (Random.int Random.minInt Random.maxInt)

-- Model
type alias Model =
    { cards : Dict.Dict Int Card.Model
    , cardOrder : List Int
    , scratchCard : CardAdder.Model
    , drawerOpen : Bool
    }

empty : Model
empty =
    { cards = Dict.empty
    , scratchCard = CardAdder.empty
    , drawerOpen = False
    , cardOrder = []
    }

addScratchCard : Model -> Model
addScratchCard model =
    let
        newId = Dict.keys model.cards
              |> List.maximum
              |> Maybe.withDefault 0
              |> (+) 1
        newCard = CardAdder.toCardModel model.scratchCard newId
    in
        { model
            | cards = (Dict.insert newId newCard model.cards)
            , scratchCard = CardAdder.empty
            , cardOrder = model.cardOrder ++ [newId]
        }

popScratchCard : Model -> Int -> Model
popScratchCard model id =
    let
        newCards = Dict.remove id model.cards
        newCardOrder = List.filter (\cid -> cid /= id) model.cardOrder
        newScratch = Dict.get id model.cards
                   |> Maybe.withDefault Card.blank
                   |> (\m -> { text = m.text
                             , color = m.color
                             , href = m.href})
    in
        { model
            | cards = newCards
            , cardOrder = newCardOrder
            , scratchCard = newScratch
            , drawerOpen = True
        }


-- Messages
type Msg = CardAdderMsg CardAdder.Msg
         | DelCard Int
         | EditCard Int
         | ToggleDrawer
         | ShuffleCards Random.Seed

-- View
cardItem : Card.Model -> Html Msg
cardItem m =
    div [class "card-item"]
        [ (Card.view m)
        , div [ class "card-controls"]
            [ button [onClick (DelCard m.id)] [text "Delete"]
            , button [onClick (EditCard m.id)] [text "Edit"]
            ]
        ]

view : Model -> Html Msg
view model =
    let
        getCard : (Int -> Card.Model)
        getCard i = Dict.get i model.cards
                  |> Maybe.withDefault Card.blank
        cards = List.map (\i -> cardItem <| getCard i) model.cardOrder
        adder = Html.map CardAdderMsg (CardAdder.view model.scratchCard)
    in
        div []
            [ div [ class (if model.drawerOpen then "open" else "closed")
                  , id "card-drawer"]
                  [ button
                        [ onClick ToggleDrawer
                        , id "toggle-button"
                        ]
                        [text (if model.drawerOpen then "▲" else "+")]
                  , div
                      [ id "card-drawer-contents"]
                      [adder]
                  ]
            , div [id "card-list"] cards
            ]


update : Msg -> Model -> (Model, Cmd Msg)
update message model =
    case message of
        ToggleDrawer -> ({model | drawerOpen = not model.drawerOpen}, Cmd.none)
        DelCard id ->
            ( {model
                 | cards = (Dict.remove id model.cards)
                 , cardOrder = List.filter (\i -> i /= id) model.cardOrder }
            , Cmd.none)
        EditCard id ->
            (popScratchCard model id, Cmd.none)
        CardAdderMsg submsg ->
            case submsg of
                CardAdder.SendCard scratch ->
                    (addScratchCard model, Cmd.none)
                _ ->
                    let
                        (newScratch, caMsg) =
                            CardAdder.update submsg model.scratchCard
                    in
                        ( {model | scratchCard = newScratch}
                        , Cmd.map CardAdderMsg caMsg)
        ShuffleCards s ->
            ( { model | cardOrder = shuffle model.cardOrder s}
            , Cmd.none)
