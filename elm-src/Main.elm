port module Main exposing (..)

import Dict
import List

import Html exposing (Html, p, div, text)
import Random

import CardList
import Card


-- Application model
type alias Model = { cardList: CardList.Model }

-- App rendering
view : Model -> Html Msg
view model =
    div []
        [ Html.map CardListMsg (CardList.view model.cardList)
        ]


-- App Initialization
type alias Flags = { cardList : List Card.Model }

init : Flags -> (Model, Cmd Msg)
init {cardList} =
    ({ cardList = CardList.fromCardList cardList }
    , Random.generate
        (\a -> CardListMsg (CardList.ShuffleCards a))
        CardList.seeder)


-- Data & Event Plumbing
type Msg
    = CardListMsg CardList.Msg
    | SetCardList (List Card.Model)

port toDB : List Card.Model -> Cmd msg
port toApp : (List Card.Model -> msg) -> Sub msg

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        CardListMsg subMsg ->
            let
                (updatedCardListModel, cardListCmd) =
                    CardList.update subMsg model.cardList
            in
                ({ model
                     | cardList = updatedCardListModel
                 }
                , Cmd.batch
                    [ Cmd.map CardListMsg cardListCmd
                    , toDB <| CardList.toCardList updatedCardListModel
                    ]
                )
        SetCardList newCards ->
            let
                oldCardList = model.cardList
                newCardListModel = { oldCardList |
                                         cards = (List.foldl
                                                      (\c d -> Dict.insert c.id c d)
                                                      Dict.empty
                                                      newCards)
                                   }
            in
                ( { model
                      | cardList = newCardListModel
                  }
                , Cmd.none)


subscriptions : Model -> Sub Msg
subscriptions m = toApp SetCardList


-- App Entrypoint
main : Program Flags Model Msg
main =
    Html.programWithFlags
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
